This NGVirtualKeyboard was built by Transonic Systems Inc. using the Qt® Community Edition.  The 5.10 keyboard library has been modified from its original form to add support for a greater range of languages.

Copyright 2020 Transonic Systems Inc.

Copyright 2016 The Qt Company Ltd.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License (LGPL) as published by the Free Software Foundation, either version 3 of the License, or, at your option, any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see https://www.gnu.org/licenses/


The Qt Virtual Keyboard is a virtual keyboard framework that consists of a C++
backend supporting custom input methods as well as a UI frontend implemented
in QML.

The original source code can be found [here](https://github.com/qt/qtvirtualkeyboard/tree/5.10).

For more information, see the [documentation](https://doc.qt.io/qt-5/qtvirtualkeyboard-index.html) for information on the latest version of this software.

For information on version 5.10 of this software see this [documentation](https://doc.qt.io/archives/qt-5.10/qtvirtualkeyboard-index.html).
