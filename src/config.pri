#
# This NGVirtualKeyboard was built by Transonic Systems Inc. using the Qt® Community Edition.  The 5.10 keyboard library has been modified from its original form to add support for a greater range of languages.
#
# Copyright 2020 Transonic Systems Inc.
#
# Copyright 2016 The Qt Company Ltd.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License (LGPL) as published by the Free Software Foundation, either version 3 of the License, or, at your option, any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see https://www.gnu.org/licenses/
#
#
# The Qt Virtual Keyboard is a virtual keyboard framework that consists of a C++
# backend supporting custom input methods as well as a UI frontend implemented
# in QML.
#
# The original source code can be found here: https://github.com/qt/qtvirtualkeyboard/tree/5.10.
#
# For more information, see the documentation: https://doc.qt.io/qt-5/qtvirtualkeyboard-index.html for information on the latest version of this software.
#
# For information on version 5.10 of this software see this https://doc.qt.io/archives/qt-5.10/qtvirtualkeyboard-index.html.
#

# Enable handwriting
handwriting:!lipi-toolkit:!t9write {
    include(virtualkeyboard/3rdparty/t9write/t9write-build.pri)
    equals(T9WRITE_FOUND, 1): CONFIG += t9write
    else: CONFIG += lipi-toolkit
}
t9write {
    !handwriting: include(virtualkeyboard/3rdparty/t9write/t9write-build.pri)
    equals(T9WRITE_CJK_FOUND, 1): CONFIG += t9write-cjk
    equals(T9WRITE_ALPHABETIC_FOUND, 1): CONFIG += t9write-alphabetic
}

# Disable built-in layouts
disable-layouts {
    message("The built-in layouts are now excluded from the Qt Virtual Keyboard plugin.")
} else {
    # Enable languages by features
    openwnn: CONFIG += lang-ja_JP
    hangul: CONFIG += lang-ko_KR
    pinyin: CONFIG += lang-zh_CN
    tcime|zhuyin|cangjie: CONFIG += lang-zh_TW

    # Default language
    !contains(CONFIG, lang-.*) {
        contains(QT_CONFIG, private_tests) { # CI or developer build, use all languages
            CONFIG += lang-all
        } else {
        CONFIG += lang-en_US  #english us not offcial
        #CONFIG += lang-bg_BG #Bulgarian - BULGARIA
        CONFIG += lang-cs_CZ #Czech - CZECHIA
        CONFIG += lang-da_DK #Danish - DENMARK
        CONFIG += lang-de_DE #German - GERMANY
        CONFIG += lang-el_GR #Modern Greek - GREECE
        CONFIG += lang-en_GB #English - Great Bitan
        CONFIG += lang-es_ES #Spanish - SPAIN
        #CONFIG += lang-et_EE #Estonian - ESTONIA
        #CONFIG += lang-fa_FA #Persian
        CONFIG += lang-fi_FI #Finnish - FINLAND
        CONFIG += lang-fr_FR #French - FRANCE
        #CONFIG += lang-he_IL #Hebrew - ISRAEL
        #CONFIG += lang-hi_IN #Hindi - INDIA
        CONFIG += lang-hr_HR #Croatian - CROATIA
        CONFIG += lang-hu_HU #Hungarian - HUNGARY
        CONFIG += lang-it_IT #Italian - ITALY
        CONFIG += lang-ja_JP #Japanese - JAPAN
        CONFIG += lang-ko_KR #Korean - KOREA
        CONFIG += lang-nb_NO #Norwegian - NORWAY
        CONFIG += lang-nl_NL #Dutch - NETHERLANDS
        CONFIG += lang-pl_PL #Polish - POLAND
        CONFIG += lang-pt_PT #Portuguese - PORTUGA
        CONFIG += lang-ro_RO #Romanian - ROMANIA
        CONFIG += lang-ru_RU #Russian - RUSSIAN
        CONFIG += lang-sr_SP #Serbian - SERBIA
        CONFIG += lang-sv_SE #Swedish - SWEDEN
        CONFIG += lang-zh_CN #Chinese - CHINA
        CONFIG += lang-zh_TW #Chinese - TAIWAN
        }
    }

    # Flag for activating all languages
    lang-all: CONFIG += \
        lang-ar_AR \
        lang-bg_BG \
        lang-cs_CZ \
        lang-da_DK \
        lang-de_DE \
        lang-el_GR \
        lang-en_GB \
        lang-es_ES \
        lang-et_EE \
        lang-fa_FA \
        lang-fi_FI \
        lang-fr_FR \
        lang-he_IL \
        lang-hi_IN \
        lang-hr_HR \
        lang-hu_HU \
        lang-it_IT \
        lang-ja_JP \
        lang-ko_KR \
        lang-nb_NO \
        lang-nl_NL \
        lang-pl_PL \
        lang-pt_PT \
        lang-ro_RO \
        lang-ru_RU \
        lang-sr_SP \
        lang-sv_SE \
        lang-zh_CN \
        lang-zh_TW
}

# Enable features by languages
contains(CONFIG, lang-ja.*)|lang-all: CONFIG += openwnn
contains(CONFIG, lang-ko.*)|lang-all: CONFIG += hangul
contains(CONFIG, lang-zh(_CN)?)|lang-all: CONFIG += pinyin
contains(CONFIG, lang-zh(_TW)?)|lang-all: CONFIG += tcime

# Feature dependencies
tcime {
    !cangjie:!zhuyin: CONFIG += cangjie zhuyin
} else {
    cangjie|zhuyin: CONFIG += tcime
}

# Deprecated configuration flags
disable-xcb {
    message("The disable-xcb option has been deprecated. Please use disable-desktop instead.")
    CONFIG += disable-desktop
}
