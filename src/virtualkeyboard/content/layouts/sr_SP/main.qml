/****************************************************************************
** This NGVirtualKeyboard was built by Transonic Systems Inc. using the Qt® Community Edition.  The 5.10 keyboard library has been modified from its original form to add support for a greater range of languages.
**
** Copyright 2020 Transonic Systems Inc.
**
** Copyright 2016 The Qt Company Ltd.
**
** This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License (LGPL) as published by the Free Software Foundation, either version 3 of the License, or, at your option, any later version.
**
** This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see https://www.gnu.org/licenses/
**
**
** The Qt Virtual Keyboard is a virtual keyboard framework that consists of a C++
** backend supporting custom input methods as well as a UI frontend implemented
** in QML.
**
** The original source code can be found here: https://github.com/qt/qtvirtualkeyboard/tree/5.10.
**
** For more information, see the documentation: https://doc.qt.io/qt-5/qtvirtualkeyboard-index.html for information on the latest version of this software.
**
** For information on version 5.10 of this software see this https://doc.qt.io/archives/qt-5.10/qtvirtualkeyboard-index.html.
****************************************************************************/

import QtQuick 2.0
import QtQuick.VirtualKeyboard 2.3

KeyboardLayoutLoader {
    sharedLayouts: ['symbols']
    sourceComponent: InputContext.inputEngine.inputMode === InputEngine.Cyrillic ? cyrillicLayout : latinLayout
    Component {
        id: cyrillicLayout
        KeyboardLayout {
            keyWeight: 160
            KeyboardRow {
                Key {
                    text: "љ"
                }
                Key {
                    text: "њ"
                }
                Key {
                    text: "е"
                }
                Key {
                    text: "р"
                }
                Key {
                    text: "т"
                }
                Key {
                    text: "з"
                }
                Key {
                    text: "у"
                }
                Key {
                    text: "и"
                }
                Key {
                    text: "о"
                }
                Key {
                    text: "п"
                }
                Key {
                    text: "ш"
                }
                BackspaceKey {}
            }
            KeyboardRow {
                FillerKey {
                    weight: 56
                }
                Key {
                    text: "а"
                }
                Key {
                    text: "с"
                }
                Key {
                    text: "д"
                }
                Key {
                    text: "ф"
                }
                Key {
                    text: "г"
                }
                Key {
                    text: "х"
                }
                Key {
                    text: "ј"
                }
                Key {
                    text: "к"
                }
                Key {
                    text: "л"
                }
                Key {
                    text: "ч"
                }
                Key {
                    text: "ћ"
                }
                EnterKey {
                    weight: 283
                }
            }
            KeyboardRow {
                keyWeight: 156
                InputModeKey {
                    inputModes: [InputEngine.Cyrillic, InputEngine.Latin]
                }
                Key {
                    text: "ѕ"
                }
                Key {
                    text: "џ"
                }
                Key {
                    text: "ц"
                }
                Key {
                    text: "в"
                }
                Key {
                    text: "б"
                }
                Key {
                    text: "н"
                }
                Key {
                    text: "м"
                }
                Key {
                    text: "ђ"
                }
                Key {
                    text: "ж"
                }
                Key {
                    key: Qt.Key_Comma
                    text: ","
                }
                Key {
                    key: Qt.Key_Period
                    text: "."
                    alternativeKeys: "!.;?"
                }
                ShiftKey {
                    weight: 204
                }
            }
            KeyboardRow {
                keyWeight: 154
                SymbolModeKey {
                    weight: 217
                }
                ChangeLanguageKey {
                    weight: 154
                }
                HandwritingModeKey {
                    weight: 154
                }
                SpaceKey {
                    weight: 864
                }
                Key {
                    key: Qt.Key_Apostrophe
                    text: "'"
                }
                SymbolModeKey {
                    
                }
                HideKeyboardKey {
                    weight: 204
                }
            }
        }
    }
    Component {
        id: latinLayout
        KeyboardLayout {
            keyWeight: 160
            KeyboardRow {
                Key {
                    key: Qt.Key_Q
                    text: "q"
                }
                Key {
                    key: Qt.Key_W
                    text: "w"
                }
                Key {
                    key: Qt.Key_E
                    text: "e"
                }
                Key {
                    key: Qt.Key_R
                    text: "r"
                }
                Key {
                    key: Qt.Key_T
                    text: "t"
                }
                Key {
                    key: Qt.Key_Z
                    text: "z"
                    alternativeKeys: "zž"
                }
                Key {
                    key: Qt.Key_U
                    text: "u"
                }
                Key {
                    key: Qt.Key_I
                    text: "i"
                }
                Key {
                    key: Qt.Key_O
                    text: "o"
                    alternativeKeys: "oö"
                }
                Key {
                    key: Qt.Key_P
                    text: "p"
                }
                BackspaceKey {}
            }
            KeyboardRow {
                FillerKey {
                    weight: 56
                }
                Key {
                    key: Qt.Key_A
                    text: "a"
                    alternativeKeys: "aåä"
                }
                Key {
                    key: Qt.Key_S
                    text: "s"
                    alternativeKeys: "sš"
                }
                Key {
                    key: Qt.Key_D
                    text: "d"
                    alternativeKeys: "dđ"
                }
                Key {
                    key: Qt.Key_F
                    text: "f"
                }
                Key {
                    key: Qt.Key_G
                    text: "g"
                }
                Key {
                    key: Qt.Key_H
                    text: "h"
                }
                Key {
                    key: Qt.Key_J
                    text: "j"
                }
                Key {
                    key: Qt.Key_K
                    text: "k"
                }
                Key {
                    key: Qt.Key_L
                    text: "l"
                }
                EnterKey {
                    weight: 283
                }
            }
            KeyboardRow {
                keyWeight: 156
                InputModeKey {
                    inputModes: [InputEngine.Cyrillic, InputEngine.Latin]
                }
                Key {
                    key: Qt.Key_Y
                    text: "y"
                }
                Key {
                    key: Qt.Key_X
                    text: "x"
                }
                Key {
                    key: Qt.Key_C
                    text: "c"
                    alternativeKeys: "ćcč"
                }
                Key {
                    key: Qt.Key_V
                    text: "v"
                }
                Key {
                    key: Qt.Key_B
                    text: "b"
                }
                Key {
                    key: Qt.Key_N
                    text: "n"
                }
                Key {
                    key: Qt.Key_M
                    text: "m"
                }
                Key {
                    key: Qt.Key_Comma
                    text: ","
                }
                Key {
                    key: Qt.Key_Period
                    text: "."
                }
                ShiftKey {
                    weight: 204
                }
            }
            KeyboardRow {
                keyWeight: 154
                SymbolModeKey {
                    weight: 217
                }
                ChangeLanguageKey {
                    weight: 154
                }
                HandwritingModeKey {
                    weight: 154
                }
                SpaceKey {
                    weight: 864
                }
                Key {
                    key: Qt.Key_Minus
                    text: "-"
                    alternativeKeys: "-\"'"
                }
                SymbolModeKey {
                    
                }
                HideKeyboardKey {
                    weight: 204
                }
            }
        }
    }
}
