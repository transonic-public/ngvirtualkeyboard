/****************************************************************************
** This NGVirtualKeyboard was built by Transonic Systems Inc. using the Qt® Community Edition.  The 5.10 keyboard library has been modified from its original form to add support for a greater range of languages.
**
** Copyright 2020 Transonic Systems Inc.
**
** Copyright 2016 The Qt Company Ltd.
**
** This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License (LGPL) as published by the Free Software Foundation, either version 3 of the License, or, at your option, any later version.
**
** This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see https://www.gnu.org/licenses/
**
**
** The Qt Virtual Keyboard is a virtual keyboard framework that consists of a C++
** backend supporting custom input methods as well as a UI frontend implemented
** in QML.
**
** The original source code can be found here: https://github.com/qt/qtvirtualkeyboard/tree/5.10.
**
** For more information, see the documentation: https://doc.qt.io/qt-5/qtvirtualkeyboard-index.html for information on the latest version of this software.
**
** For information on version 5.10 of this software see this https://doc.qt.io/archives/qt-5.10/qtvirtualkeyboard-index.html.
****************************************************************************/

import QtQuick 2.0
import QtQuick.VirtualKeyboard 2.3

KeyboardLayoutLoader {
    inputMode: InputEngine.Arabic
    sourceComponent: InputContext.shift ? page2 : page1
    Component {
        id: page1
        KeyboardLayout {
            keyWeight: 160
            KeyboardRow {
                Key {
                    text: "\u0636"
                }
                Key {
                    text: "\u0635"
                }
                Key {
                    text: "\u062B"
                }
                Key {
                    text: "\u0642"
                }
                Key {
                    text: "\u0641"
                }
                Key {
                    text: "\u063A"
                }
                Key {
                    text: "\u0639"
                }
                Key {
                    text: "\u0647"
                }
                Key {
                    text: "\u062E"
                }
                Key {
                    text: "\u062D"
                }
                Key {
                    text: "\u062C"
                }
                Key {
                    text: "\u0686"
                }
                BackspaceKey {}
            }
            KeyboardRow {
                FillerKey {
                    weight: 56
                }
                Key {
                    text: "\u0634"
                }
                Key {
                    text: "\u0633"
                }
                Key {
                    text: "\u06CC"
                }
                Key {
                    text: "\u0628"
                }
                Key {
                    text: "\u0644"
                }
                Key {
                    text: "\u0627"
                }
                Key {
                    text: "\u062A"
                }
                Key {
                    text: "\u0646"
                }
                Key {
                    text: "\u0645"
                }
                Key {
                    text: "\u06A9"
                }
                Key {
                    text: "\u06AF"
                }
                EnterKey {
                    weight: 283
                }
            }
            KeyboardRow {
                keyWeight: 156
                ShiftKey {}
                Key {
                    text: "\u0638"
                }
                Key {
                    text: "\u0637"
                }
                Key {
                    text: "\u0632"
                }
                Key {
                    text: "\u0631"
                }
                Key {
                    text: "\u0630"
                }
                Key {
                    text: "\u062F"
                }
                Key {
                    text: "\u067E"
                }
                Key {
                    text: "\u0648"
                }
                Key {
                    key: 0x060C
                    text: "\u060C"
                    alternativeKeys: "\u060C,"
                }
                Key {
                    key: Qt.Key_Period
                    text: "."
                }
                ShiftKey {
                    weight: 204
                }
            }
            KeyboardRow {
                keyWeight: 154
                SymbolModeKey {
                    weight: 217
                    displayText: "\u06F1\u06F2\u06F3\u061F"
                }
                ChangeLanguageKey {
                    weight: 154
                }
                HandwritingModeKey {
                    weight: 154
                }
                SpaceKey {
                    weight: 864
                }
                Key {
                    text: "\u200D"
                    displayText: "ZWJ"
                }
                SymbolModeKey {
                    
                }
                HideKeyboardKey {
                    weight: 204
                }
            }
        }
    }
    Component {
        id: page2
        KeyboardLayout {
            keyWeight: 160
            KeyboardRow {
                Key {
                    text: "\u0652"
                }
                Key {
                    text: "\u064C"
                }
                Key {
                    text: "\u064D"
                }
                Key {
                    text: "\u064B"
                }
                Key {
                    text: "\u064F"
                }
                Key {
                    text: "\u0650"
                }
                Key {
                    text: "\u064E"
                }
                Key {
                    text: "\u0651"
                }
                Key {
                    enabled: false
                }
                Key {
                    enabled: false
                }
                Key {
                    enabled: false
                }
                Key {
                    enabled: false
                }
                BackspaceKey {}
            }
            KeyboardRow {
                FillerKey {
                    weight: 56
                }
                Key {
                    enabled: false
                }
                Key {
                    text: "\u064F"
                }
                Key {
                    text: "\u064A"
                }
                Key {
                    text: "\u0625"
                }
                Key {
                    text: "\u0623"
                }
                Key {
                    text: "\u0622"
                }
                Key {
                    text: "\u0629"
                }
                Key {
                    enabled: false
                }
                Key {
                    enabled: false
                }
                Key {
                    enabled: false
                }
                Key {
                    enabled: false
                }
                EnterKey {
                    weight: 283
                }
            }
            KeyboardRow {
                keyWeight: 156
                ShiftKey {}
                Key {
                    text: "\u0643"
                }
                Key {
                    text: "\u0653"
                }
                Key {
                    text: "\u0698"
                }
                Key {
                    text: "\u0670"
                }
                Key {
                    text: "\u0621"
                }
                Key {
                    enabled: false
                }
                Key {
                    enabled: false
                }
                Key {
                    text: "\u0624"
                }
                Key {
                    key: 0x060C
                    text: "\u060C"
                    alternativeKeys: "\u060C,"
                }
                Key {
                    key: Qt.Key_Period
                    text: "."
                }
                ShiftKey {
                    weight: 204
                }
            }
            KeyboardRow {
                keyWeight: 154
                SymbolModeKey {
                    weight: 217
                    displayText: "\u06F1\u06F2\u06F3\u061F"
                }
                ChangeLanguageKey {
                    weight: 154
                }
                HandwritingModeKey {
                    weight: 154
                }
                SpaceKey {
                    weight: 864
                }
                Key {
                    text: "\u200C"
                    displayText: "ZWNJ"
                }
                SymbolModeKey {
                    
                }
                HideKeyboardKey {
                    weight: 204
                }
            }
        }
    }
}
