/****************************************************************************
** This NGVirtualKeyboard was built by Transonic Systems Inc. using the Qt® Community Edition.  The 5.10 keyboard library has been modified from its original form to add support for a greater range of languages.
**
** Copyright 2020 Transonic Systems Inc.
**
** Copyright 2016 The Qt Company Ltd.
**
** This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License (LGPL) as published by the Free Software Foundation, either version 3 of the License, or, at your option, any later version.
**
** This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see https://www.gnu.org/licenses/
**
**
** The Qt Virtual Keyboard is a virtual keyboard framework that consists of a C++
** backend supporting custom input methods as well as a UI frontend implemented
** in QML.
**
** The original source code can be found here: https://github.com/qt/qtvirtualkeyboard/tree/5.10.
**
** For more information, see the documentation: https://doc.qt.io/qt-5/qtvirtualkeyboard-index.html for information on the latest version of this software.
**
** For information on version 5.10 of this software see this https://doc.qt.io/archives/qt-5.10/qtvirtualkeyboard-index.html.
****************************************************************************/

import QtQuick 2.0
import QtQuick.VirtualKeyboard 2.1

KeyboardLayoutLoader {
    function createInputMethod() {
        return Qt.createQmlObject('import QtQuick 2.0; import QtQuick.VirtualKeyboard 2.1; HangulInputMethod {}', parent, "hangulInputMethod")
    }
    sourceComponent: InputContext.shift ? page2 : page1
    sharedLayouts: ['symbols']
    Component {
        id: page1
        KeyboardLayout {
            keyWeight: 160
            KeyboardRow {
                Key {
                    text: "\u3142"
                }
                Key {
                    text: "\u3148"
                }
                Key {
                    text: "\u3137"
                }
                Key {
                    text: "\u3131"
                }
                Key {
                    text: "\u3145"
                }
                Key {
                    text: "\u315B"
                }
                Key {
                    text: "\u3155"
                }
                Key {
                    text: "\u3151"
                }
                Key {
                    text: "\u3150"
                }
                Key {
                    text: "\u3154"
                }
                BackspaceKey {}
            }
            KeyboardRow {
                FillerKey {
                    weight: 56
                }
                Key {
                    text: "\u3141"
                }
                Key {
                    text: "\u3134"
                }
                Key {
                    text: "\u3147"
                }
                Key {
                    text: "\u3139"
                }
                Key {
                    text: "\u314E"
                }
                Key {
                    text: "\u3157"
                }
                Key {
                    text: "\u3153"
                }
                Key {
                    text: "\u314F"
                }
                Key {
                    text: "\u3163"
                }
                EnterKey {
                    weight: 283
                }
            }
            KeyboardRow {
                keyWeight: 156
                ShiftKey {}
                Key {
                    text: "\u314B"
                }
                Key {
                    text: "\u314C"
                }
                Key {
                    text: "\u314A"
                }
                Key {
                    text: "\u314D"
                }
                Key {
                    text: "\u3160"
                }
                Key {
                    text: "\u315C"
                }
                Key {
                    text: "\u3161"
                }
                Key {
                    key: Qt.Key_Comma
                    text: ","
                }
                Key {
                    key: Qt.Key_Period
                    text: "."
                }
                ShiftKey {
                    weight: 204
                }
            }
            KeyboardRow {
                keyWeight: 154
                SymbolModeKey {
                    weight: 217
                }
                ChangeLanguageKey {
                    weight: 154
                }
                HandwritingModeKey {
                    weight: 154
                }
                SpaceKey {
                    weight: 864
                }
                Key {
                    key: Qt.Key_Apostrophe
                    text: "'"
                }
                SymbolModeKey {
                    
                }
                HideKeyboardKey {
                    weight: 204
                }
            }
        }
    }
    Component {
        id: page2
        KeyboardLayout {
            keyWeight: 160
            KeyboardRow {
                Key {
                    text: "\u3143"
                }
                Key {
                    text: "\u3149"
                }
                Key {
                    text: "\u3138"
                }
                Key {
                    text: "\u3132"
                }
                Key {
                    text: "\u3146"
                }
                Key {
                    text: "\u315B"
                }
                Key {
                    text: "\u3155"
                }
                Key {
                    text: "\u3151"
                }
                Key {
                    text: "\u3152"
                }
                Key {
                    text: "\u3156"
                }
                BackspaceKey {}
            }
            KeyboardRow {
                FillerKey {
                    weight: 56
                }
                Key {
                    text: "\u3141"
                }
                Key {
                    text: "\u3134"
                }
                Key {
                    text: "\u3147"
                }
                Key {
                    text: "\u3139"
                }
                Key {
                    text: "\u314E"
                }
                Key {
                    text: "\u3157"
                }
                Key {
                    text: "\u3153"
                }
                Key {
                    text: "\u314F"
                }
                Key {
                    text: "\u3163"
                }
                EnterKey {
                    weight: 283
                }
            }
            KeyboardRow {
                keyWeight: 156
                ShiftKey {}
                Key {
                    text: "\u314B"
                }
                Key {
                    text: "\u314C"
                }
                Key {
                    text: "\u314A"
                }
                Key {
                    text: "\u314D"
                }
                Key {
                    text: "\u3160"
                }
                Key {
                    text: "\u315C"
                }
                Key {
                    text: "\u3161"
                }
                Key {
                    key: Qt.Key_Comma
                    text: ","
                }
                Key {
                    key: Qt.Key_Period
                    text: "."
                }
                ShiftKey {
                    weight: 204
                }
            }
            KeyboardRow {
                keyWeight: 154
                SymbolModeKey {
                    weight: 217
                }
                ChangeLanguageKey {
                    weight: 154
                }
                HandwritingModeKey {
                    weight: 154
                }
                SpaceKey {
                    weight: 864
                }
                Key {
                    key: Qt.Key_Apostrophe
                    text: "'"
                }
                SymbolModeKey {
                    
                }
                HideKeyboardKey {
                    weight: 204
                }
            }
        }
    }
}
