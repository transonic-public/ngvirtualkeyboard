/****************************************************************************
** This NGVirtualKeyboard was built by Transonic Systems Inc. using the Qt® Community Edition.  The 5.10 keyboard library has been modified from its original form to add support for a greater range of languages.
**
** Copyright 2020 Transonic Systems Inc.
**
** Copyright 2016 The Qt Company Ltd.
**
** This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License (LGPL) as published by the Free Software Foundation, either version 3 of the License, or, at your option, any later version.
**
** This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see https://www.gnu.org/licenses/
**
**
** The Qt Virtual Keyboard is a virtual keyboard framework that consists of a C++
** backend supporting custom input methods as well as a UI frontend implemented
** in QML.
**
** The original source code can be found here: https://github.com/qt/qtvirtualkeyboard/tree/5.10.
**
** For more information, see the documentation: https://doc.qt.io/qt-5/qtvirtualkeyboard-index.html for information on the latest version of this software.
**
** For information on version 5.10 of this software see this https://doc.qt.io/archives/qt-5.10/qtvirtualkeyboard-index.html.
****************************************************************************/

import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.VirtualKeyboard 2.1

KeyboardLayoutLoader {
    function createInputMethod() {
        return Qt.createQmlObject('import QtQuick 2.0; import QtQuick.VirtualKeyboard 2.1; TCInputMethod {}', parent, "tcInputMethod")
    }
    sharedLayouts: ['main']
    property int page
    readonly property int numPages: 3
    property var keysPage1: [
        "1234567890",
        "@#$%^&*()",
        "“”、=：；！？～"
    ]
    property var keysPage2: [
        "-+/\\|[]{}·",
        "<>,.:;!?~",
        "／\"'_§￥€£¢"
    ]
    property var keysPage3: [
        "（）〔〕〈〉《》【】",
        "→←↑↓↔■□●○",
        "＼『』「」★☆◆◇"
    ]
    sourceComponent: {
        switch (page) {
        case 2: return page3
        case 1: return page2
        default: return page1
        }
    }
    Component {
        id: page1
        KeyboardLayout {
            keyWeight: 160
            KeyboardRow {
                Repeater {
                    model: keysPage1[0].length
                    Key {
                        key: keysPage1[0][index].charCodeAt(0)
                        text: keysPage1[0][index]
                    }
                }
                BackspaceKey {}
            }
            KeyboardRow {
                FillerKey {
                    weight: 56
                }
                Repeater {
                    model: keysPage1[1].length
                    Key {
                        key: keysPage1[1][index].charCodeAt(0)
                        text: keysPage1[1][index]
                    }
                }
                EnterKey {
                    weight: 283
                }
            }
            KeyboardRow {
                keyWeight: 156
                Key {
                    displayText: (page + 1) + "/" + numPages
                    functionKey: true
                    onClicked: page = (page + 1) % numPages
                }
                Repeater {
                    model: keysPage1[2].length
                    Key {
                        key: keysPage1[2][index].charCodeAt(0)
                        text: keysPage1[2][index]
                    }
                }
                Key {
                    weight: 204
                    displayText: (page + 1) + "/" + numPages
                    functionKey: true
                    onClicked: page = (page + 1) % numPages
                }
            }
            KeyboardRow {
                keyWeight: 154
                SymbolModeKey {
                    weight: 217
                    displayText: "ABC"
                }
                ChangeLanguageKey {
                    weight: 154
                }
                SpaceKey {
                    weight: 864
                }
                Key {
                    key: 0x2014
                    text: "—"
                }
                SymbolModeKey {
                    displayText: "ABC"
                }
                HideKeyboardKey {
                    weight: 204
                }
            }
        }
    }
    Component {
        id: page2
        KeyboardLayout {
            keyWeight: 160
            KeyboardRow {
                Repeater {
                    model: keysPage2[0].length
                    Key {
                        key: keysPage2[0][index].charCodeAt(0)
                        text: keysPage2[0][index]
                    }
                }
                BackspaceKey {}
            }
            KeyboardRow {
                FillerKey {
                    weight: 56
                }
                Repeater {
                    model: keysPage2[1].length
                    Key {
                        key: keysPage2[1][index].charCodeAt(0)
                        text: keysPage2[1][index]
                    }
                }
                EnterKey {
                    weight: 283
                }
            }
            KeyboardRow {
                keyWeight: 156
                Key {
                    displayText: (page + 1) + "/" + numPages
                    functionKey: true
                    onClicked: page = (page + 1) % numPages
                }
                Repeater {
                    model: keysPage2[2].length
                    Key {
                        key: keysPage2[2][index].charCodeAt(0)
                        text: keysPage2[2][index]
                    }
                }
                Key {
                    weight: 204
                    displayText: (page + 1) + "/" + numPages
                    functionKey: true
                    onClicked: page = (page + 1) % numPages
                }
            }
            KeyboardRow {
                keyWeight: 154
                SymbolModeKey {
                    weight: 217
                    displayText: "ABC"
                }
                ChangeLanguageKey {
                    weight: 154
                }
                SpaceKey {
                    weight: 864
                }
                Key {
                    key: 0x3002
                    text: "。"
                }
                SymbolModeKey {
                    displayText: "ABC"
                }
                HideKeyboardKey {
                    weight: 204
                }
            }
        }
    }
    Component {
        id: page3
        KeyboardLayout {
            keyWeight: 160
            KeyboardRow {
                Repeater {
                    model: keysPage3[0].length
                    Key {
                        key: keysPage3[0][index].charCodeAt(0)
                        text: keysPage3[0][index]
                    }
                }
                BackspaceKey {}
            }
            KeyboardRow {
                FillerKey {
                    weight: 56
                }
                Repeater {
                    model: keysPage3[1].length
                    Key {
                        key: keysPage3[1][index].charCodeAt(0)
                        text: keysPage3[1][index]
                    }
                }
                EnterKey {
                    weight: 283
                }
            }
            KeyboardRow {
                keyWeight: 156
                Key {
                    displayText: (page + 1) + "/" + numPages
                    functionKey: true
                    onClicked: page = (page + 1) % numPages
                }
                Repeater {
                    model: keysPage3[2].length
                    Key {
                        key: keysPage3[2][index].charCodeAt(0)
                        text: keysPage3[2][index]
                    }
                }
                Key {
                    weight: 204
                    displayText: (page + 1) + "/" + numPages
                    functionKey: true
                    onClicked: page = (page + 1) % numPages
                }
            }
            KeyboardRow {
                keyWeight: 154
                SymbolModeKey {
                    weight: 217
                    displayText: "ABC"
                }
                ChangeLanguageKey {
                    weight: 154
                }
                SpaceKey {
                    weight: 864
                }
                Key {
                    key: 0x2026
                    text: "…"
                }
                SymbolModeKey {
                    displayText: "ABC"
                }
                HideKeyboardKey {
                    weight: 204
                }
            }
        }
    }
}
